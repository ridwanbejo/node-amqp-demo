var amqp = require('amqplib/callback_api');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

amqp.connect('amqp://192.168.99.100:32771', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'hello';

    ch.assertQueue(q, {durable: false});

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
	
	setTimeout(function() {

		ch.consume(q, function(msg) {

		  console.log(" [x] Received %s", msg.content.toString());


		}, {noAck: true});

	}, getRandomInt(10) * 10000 );
  });
});