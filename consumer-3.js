var amqp = require('amqplib/callback_api');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

amqp.connect('amqp://192.168.99.100:32769', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var ex = 'logs';

    ch.assertExchange(ex, 'fanout', {durable: false});

    ch.assertQueue('', {exclusive: true}, function(err, q){
        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
        ch.bindQueue(q.queue, ex, '');

        ch.consume(q.queue, function(msg) {
          console.log(" [x] Received %s", msg.content.toString());
        }, {noAck: true});
    });
  });
});