var amqp = require('amqplib/callback_api');
var elasticsearch = require("elasticsearch");
var client = new elasticsearch.Client({
    host: "localhost:9200",
    log: "trace"
});

const uuidV4 = require('uuid/v4');

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

amqp.connect('amqp://192.168.99.100:32769', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'clickstreamLogQueue';

    ch.assertQueue(q, {durable: false});
    ch.prefetch(1);

    console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", q);
	
	ch.consume(q, function(msg) {

      console.log("Received: %s", msg.content.toString());
      
      var payload = JSON.parse(msg.content.toString());
      console.log(payload);

      client.index({  
        index: 'mylog',
        id: uuidV4(),
        type: "clickstreamLog",
        body: {
            mouse_position_x: payload.mouse_position_x,
            mouse_position_y: payload.mouse_position_y,
            sourceUrl: payload.sourceUrl,
            userId: payload.userId,
            createdAt: "2017-03-05"
        }
      },function(err,resp,status) {
        if(err) {
          console.log(err);
        }
        else {
          console.log("create",resp);
        }
      });

      setTimeout(function() {
        console.log(" [x] Done");
        ch.ack(msg);
      },1000);

    }, {noAck: false});

  });
});