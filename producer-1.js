var amqp = require('amqplib/callback_api');



amqp.connect('amqp://192.168.99.100:32771', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'hello';

    ch.assertQueue(q, {durable: false});
    
    // Note: on Node 6 Buffer.from(msg) should be used

    for (var i = 0; i < 10; i ++)
    {	
    	console.log(i);

    	ch.sendToQueue(q, new Buffer('Hello World! ' + i.toString() ));

    }
    
    
    console.log(" [x] Sent 'Hello World!'");
  });
});