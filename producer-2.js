var amqp = require('amqplib/callback_api');


amqp.connect('amqp://192.168.99.100:32769', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'task_queue';
    var msg = process.argv.slice(2).join(' ')+Date.now() || "Hello World!"+Date.now();

    ch.assertQueue(q, {durable: false});
    ch.sendToQueue(q, new Buffer(msg), {persistent: true});

	console.log(" [x] Sent '"+msg+"' ");
  });

  setTimeout(function() { 
  	conn.close(); 
  	process.exit(0) 
  }, 100);
});