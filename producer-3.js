var amqp = require('amqplib/callback_api');


amqp.connect('amqp://192.168.99.100:32769', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var ex = 'logs';
    var msg = process.argv.slice(2).join(' ') || "Hello World!";

    // this lines for creating publisher
    ch.assertExchange(ex, 'fanout', {durable: false});
    ch.publish(ex, '', new Buffer(msg));

	  console.log(" [x] Sent '"+msg+"' ");
  });

  setTimeout(function() { 
  	conn.close(); 
  	process.exit(0) 
  }, 10);
});