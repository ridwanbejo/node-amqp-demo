var amqp = require('amqplib/callback_api');


amqp.connect('amqp://192.168.99.100:32769', function(err, conn) {
  conn.createChannel(function(err, ch) {
    var q = 'clickstreamLogQueue';
    var msg = JSON.stringify({
		"mouse_position_x":35, 
		"mouse_position_y":240, 
		"sourceUrl": "http://localhost:8000/register", 
		"userId": "12345",
		"createdAt":"2018-01-10 01:00:00"
	});

    ch.assertQueue(q, {durable: false});
    ch.sendToQueue(q, new Buffer(msg), {persistent: true});

	console.log(" [x] Sent '"+msg+"' ");
  });

  setTimeout(function() { 
  	conn.close(); 
  	process.exit(0) 
  }, 100);
});